
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  mode:'development',
  entry: {
    // 新增的组件都要写到这里
    mytitle: './src/components/mytitle/index.js',
    card: './src/components/card/index.js',
    index: './src/components/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].umd.js',
    library: 'yz-ui', // 组件库
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader','vue-style-loader', 'css-loader'],
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}