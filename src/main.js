import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// 引入组件 安装
// import mytitle from './components/mytitle/index.js'
// import mytitle from '../dist/mytitle.umd.js'

import allComp from './components/index.js'
const { mytitle, card } = allComp
Vue.use(mytitle)
Vue.use(card)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
