// 引入vue组件
import mytitle from './index.vue'

// 要提供一个install，将来给Vue.use()函数调用的
mytitle.install = function (Vue) {
  // 注册组件
  Vue.component(mytitle.name, mytitle)
}

export default mytitle