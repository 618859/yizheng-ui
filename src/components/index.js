import cardComp from './card/index.vue';
import mytitleComp from './mytitle/index.vue';

import card from './card/index.js';
import mytitle from './mytitle/index.js';

const component = [cardComp, mytitleComp]

const install = (Vue) =>{
  component.map(item =>{
    Vue.component(item.name, item)
  })
}

export default {
  install,
  card,
  mytitle
}