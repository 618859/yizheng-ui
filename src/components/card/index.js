// 引入vue组件
import card from './index.vue'

// 要提供一个install，将来给Vue.use()函数调用的
card.install = function (Vue) {
  // 注册组件
  Vue.component(card.name, card)
}

export default card